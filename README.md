# The House Organ

The House Organ is a DIY label releasing exploratory music.

# This Repository

I'm building a new website a) as an excuse to migrate from Jekyll to Hugo, b) as an excersize in learning some new skills, and c) to actualise a bit of a resign I prototyped last year.

There's no specific map here, progress will be sporadic, uneven, and likely to change/break.

Still, if anything os of use, it's free for you to use (under MIT license)
