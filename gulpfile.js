var gulp = require('gulp');
var postcss = require('gulp-postcss');
var cssImport = require('postcss-import');
var cssFontMagic = require('postcss-font-magician');
var autoprefixer = require('autoprefixer');
var presetEnv = require('postcss-preset-env');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var purgecss = require('@fullhuman/postcss-purgecss')

const cssSourceFile = 'resources/css/style.css';
const outputFolder = 'assets/css';
const watchedResources = 'resources/css/**/*';

const { src, dest } = require("gulp");
const watchedImages = 'assets/images/**/*.{jpg,jpeg,png}';
const sharpResponsive = require("gulp-sharp-responsive");

gulp.task('styles', function(done){
 return gulp.src(cssSourceFile)
  .pipe(sourcemaps.init())
  .pipe(postcss([
    cssImport(),
    cssFontMagic({
      display: 'swap'
   }),
    purgecss({
          content: ['**/*.html']
      }),
    autoprefixer(),
//    require('postcss-initial')(),
//    require('postcss-autoreset')(),
    presetEnv({
      stage: 1,
      features: {
        'nesting-rules': true
      },
      browsers: 'last 2 versions'
    })
]))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(outputFolder))
  .on('end', done);
});

gulp.task('images', function(done){
  return gulp
  .src("assets/images/**/*.{jpg,jpeg,png}")
  .pipe(sharpResponsive({
    formats: [
      // jpeg,png
      { width: 600, rename: { suffix: "-w_600" } },
      { width: 300, rename: { suffix: "-w_300" } },
      { width: 200, rename: { suffix: "-w_200" } },
      // webp
      { width: 600, format: "webp", rename: { suffix: "-w_600" } },
      { width: 300, format: "webp", rename: { suffix: "-w_300" } },
      { width: 200, format: "webp", rename: { suffix: "-w_200" } },
      // avif
      { width: 600, format: "avif", rename: { suffix: "-w_600" } },
      { width: 300, format: "avif", rename: { suffix: "-w_300" } },
      { width: 200, format: "avif", rename: { suffix: "-w_200" } },
    ]
  }))
  .pipe(dest("static/images"));
});

gulp.task('watch', gulp.series('styles', function(done){
  gulp.watch(watchedResources, gulp.parallel('styles'));
  gulp.watch(watchedImages, {events: ['add']}, gulp.parallel('images'));
  done();
}));

gulp.task('default', gulp.series('watch', function () {}));