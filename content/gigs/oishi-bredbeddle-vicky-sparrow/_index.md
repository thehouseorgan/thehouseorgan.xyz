+++
title = "Oishi / Bredbeddle / Vicky Sparrow"
date = "2023-10-15"
publishdate = "2023-10-03"
poster = "OS-BB-VS-GigPoster.png"
image = "OS-BB-VS-GigSquare.png"
imgalt = "Gig poster. Close-up of left-over painting marks on a studio wall taken from a sharp angle blurring much of the image into the background. All rendered in greyscale. Text as elaborated in the post content."
+++

Sun 15th October, 8pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
£8 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/595760/
* https://gath.io/B26MeuWyULX78arzl18HR
* https://www.facebook.com/events/211000335331495



### OISHI
Hardware / software electronic improvisation duo using samples and their own materials, field recordings, lives etc. Setup includes mixers, laptops, cassette players, etc. Slightly different or very different every time.

Zheng Hao and Ren Shang are two artists from China, currently-based in London, UK. Their music as Oishi is a playful, joyful, and at points absurdist exploration through musique concrete, diaristic field recordings and digitally augmented realities.

https://oishi.bandcamp.com/

### BREDBEDDLE
Bredbeddle is Rebecca Lee, an artist, composer and musician based in Nottingham, UK. Her work merges composition and collage. Fragments from her record collection, found sounds, and diaristic recordings become parts of a score, building blocks to be written onto a timeline to create something new. While perhaps reminiscent of plunderphonics aesthetically, there is a musicality and narrative coherence to her songs that is distinct from an aleatoric or poetic montaging of sounds.

Strands of harmony and melody, of similarity and juxtaposition, are pulled from unexpected places. Held up for contemplation and then strung together into intricate pieces. Often manifesting as long-form, durational compositions, Bredbeddle’s process is rooted in seeing archives as a launching off point. Recorded material as a site of potential rather than an end point frozen in time and context.

https://www.rebeccalee.info/

### VICKY SPARROW
Vicky Sparrow is a Nottingham-based poet, teacher, researcher, and performer specialising in poetics. She currently teaches Creative Writing at the University of Nottingham, and edits reviews for the Journal of British and Irish Innovative Poetry. She holds a PhD on the poet-activist Anna Mendelssohn and was recently a fellow at the Wellcome Trust, researching the intersections between poetry and therapy. Sparrow’s poems can be found in Front Horse, Writing Utopia, datableed, Litmus and elsewhere. Her debut poetry pamphlet Notes to Selves (2016) is published by Zarf Editions.

https://www.criticalpoetics.co.uk/publications/vicky-sparrow/