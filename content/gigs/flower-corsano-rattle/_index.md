+++
title = "Flower-Corsano Duo / Rattle"
date = "2023-08-28"
publishdate = "2023-08-03"
poster = "FC-RT-GigPoster.png"
image = "FC-RT-GigSquare.png"
imgalt = "Gig poster. A weird, grainy, wavy, tentacular, inky, twisted loop of a blob against a plain background. All rendered in greyscale. Text as elaborated in the post content."
+++

Mon 28th September, 2pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
BYOB
£8 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/589484
* https://gath.io/2QNiX2NwEXnE5lql08x1T
* https://www.facebook.com/events/6184275888366828
* https://www.rammelclub.org/2023/08/rammel-club-121-flower-corsano-duo.html

### FLOWER-CORSANO DUO
Formed in 2005, the duo melds the propane-lit, overdriven drone-ragas of MICHAEL FLOWER (Vibracathedral Orchestra, MV & EE, Sunburned Hand of the Man)’s electrified shahi baaja and the melodically kinetic and free drumming of CHRIS CORSANO (Paul Flaherty, Joe McPhee, Rangda, Bjork). In the years since their debut album The Radiant Mirror (Textile, 2007), they have toured and recorded frequently, expanding their range to include a mind-boggling array of free sound with seamless shifts between tumultuous intensity and blissed-out serenity.
https://vhfrecords.bandcamp.com/album/the-halcyon

### RATTLE
Rattle is an ongoing musical project concerned with experiments in rhythm, metre and tension. Katharine Eira Brown & Theresa Wrigley make up the duo formed in Nottingham with drums and occasional vocalisations weaving an expanse of percussive vortices. The drum beats phase and sidestep, they trade accents and overlap, providing a suitably alive terrain for the vocals to explore similar tendencies of pattern. Rattle's debut album was released in 2016 through Upset The Rhythm / I Own You. We're excited to have them back to Rammel Club.
https://rattleon.bandcamp.com/album/sequence