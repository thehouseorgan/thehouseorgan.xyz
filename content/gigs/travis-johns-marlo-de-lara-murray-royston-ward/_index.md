+++
title = "Travis Johns / Marlo De Lara / Murray Royston-Ward"
date = "2023-05-28"
publishdate = "2023-04-16"
poster = "TravisJohnsMarloGigPoster.png"
image = "TravisJohnsMarloGigSquare.png"
imgalt = "Gig poster. Distorted (manipulated scan) image of a small statue head originally photographed in a museum in Jordan. Jagged wavy contours with an eye and shoulder poking out at the end. All rendered in greyscale. Text as elaborated in the post content."
+++

Sun 28th May 2023, 7:30pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
BYOB
£7 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/579370/
* https://gath.io/PMlKM91xYJBAVw9KA5lJE
* https://www.facebook.com/events/240326681870697/

### TRAVIS JOHNS

Sound-artist hailing from Ithaca, NY. On tour: Improvised, electro-acoustic, brain-wave conversion. As Vauxflores: purveyor of oddball circuitry (shared territories w/ Ciat-Lonbarde/David Tudor/Don Buchla, and SF underground scene, BrutalSFX. Part of a fine experimental music lineage through Oberlin and Mills colleges.

https://vauxflores.com/

### MARLO DE LARA

Micro-cosmic tone-scapes sculpted into densely cathartic noise-expanse. Multi-disciplinary sound/art/activism, stalwart of experimental scenes in Baltimore, Leeds, and Beyond. Ladyz in Noyz, full-blown academia and community building heavyweight, the artist formerly known as Marlo Eggplant.

https://marlodelara.squarespace.com

### MURRAY ROYSTON-WARD

Deep-dive snare feedback collaborating with wonky circuits. Tripping on Wires/The House Organ/Jehova’s Fitness/et al.

https://mroystonward.com