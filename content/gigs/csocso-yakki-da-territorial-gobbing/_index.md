+++
title = "Csocsó / Yakki Da! / Territorial Gobbing"
date = "2023-09-10"
publishdate = "2023-07-28"
poster = "CS-YD-TG-GigPoster.png"
image = "CS-YD-TG-GigSquare.png"
imgalt = "Gig poster. Grainy, wobbly, amorphous scanner blob is floating, smeared above another scanner artefact (where the shadow might fall). All rendered in greyscale. Text as elaborated in the post content."
+++

Sun 9th September, 7:30pm
[The Chameleon, Nottingham](https://www.openstreetmap.org/way/120036329#map=19/52.95342/-1.15215)
£7 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/589106/
* https://gath.io/6e_nK54ImoWsbhrk1HN1T
* https://www.facebook.com/events/3465317730465129

### CSOCSÓ
Bruxelles-based Danielle Dariel seems to operate under numerous modes and projects. Groaning skittery clicks, cuts, bleeps, bloops, and electronic atmos swashbuckling from the desynchronised depths of post-club adjacent noise. Other projects include Visage Pâle, Morosphinx, Umarell & Zdaura, Donna Candy, and Wash Club (w/ Új Bála who you hopefully caught at the recent Rammel Club).
https://soundcloud.com/csocso
https://b0-0b.bandcamp.com/album/z-dcptive-sessions-vp-bob
https://biometapes1.bandcamp.com/album/-
https://washclub.bandcamp.com/album/les-ateliers-claus-13052023

### YAKKI DA!
Experimental karaoke, ASMR, party drone from Leeds. Hils (Cowtown) brings joyful fun times and zone-out casio meditation. Also found as Basic Switches and (with Theo, see below) Garbage Pail Kids.
https://yakkida.bandcamp.com
https://www.youtube.com/watch?v=7vzRKftHSj0

### TERRITORIAL GOBBING
Ridiculously prolific dada-noise-improv-junk from Leeds. Last time we saw Theo he was sporting a minor injury (which didn’t slow him down one bit) so this time I’m expecting two-handed tape skronk, FX detritus, wobbly table legs, and goofball theatrics.
https://territorialgobbing.bandcamp.com/
https://territorialgobbing.neocities.org/