+++
title = "LuxuryMollusc / Nacht Und Nebel / Blackcloudsummoner"
date = "2019-10-06"
publishdate = "2019-09-06"
poster = "LuxuryMolluscPoster.png"
image = "LuxuryMolluscPosterSquare.png"
imgalt = "Gig poster. Unknown metal framed tower on the coast at Lindisfarne with a triangle shape. The shot is dominated by light grey clouds in the background, hovering moodily above a thin ribbon of coastline. All rendered in greyscale. Text as elaborated in the post content."
+++

Doom-laden electronics, drones, and noise...

Sun 6th October 2019, 3pm  
[Jabbarwocky](https://www.openstreetmap.org/way/1081157697)    
£6 Donantion  
No-one turned away for lack of funds

### LUXURYMOLLUSC (Dublin, Ireland)
https://luxuriousmollusc.bandcamp.com

Negative experiments for spiritual castration via atmospheric noise walls, manipulated field recordings, and detritus divinations.

### NACHT UND NEBEL (Nottingham, UK)
https://nachtundnebel.bandcamp.com/

Solo guise of Henry Davies (Bloody Head, Molloch, Nadir) extracting doomy, thick, black, sludge from cello recordings.

### BLACKCLOUDSUMMONER (Nottingham, UK)
https://blackcloudsummoner.bandcamp.com

Ritualistic globules of bass, dirt electronics, and a surface of slime flecked with cosmic dust.

### SHOW DETAILS

This is an afternoon show, full timings will be announced soon but doors open at 3pm and expect things to be wrapped up by 7pm.

The bar will remain open to the public but suggested donations of £6 will be asked for, to help fund artist travel/expenses. No-one will be turned away for lack of funds though.

THE HOUSE ORGAN is a DIY label for audio and other artifacts spanning noise and improvisation. For audio releases and more information https://thehouseorgan.xyz