+++
title = "Kazehito Seki / SW1n-Hunter / Giblet Gusset"
date = "2019-12-09"
publishdate = "2019-10-09"
poster = "KazehitoSekiPoster.png"
image = "KazehitoSekiPosterSquare.jpeg"
imgalt = "Gig poster. A large, unorganized, tightly packed collection of old sculpted heads found at the back of an art university in Dhaka. The heads are interspersed with a headless bust, a precariously balanced stone chair atop a pile of sculptures, and unknown objects and materials. The centre of the shot is domiated by a gaunt-looking, haunting head. All rendered in greyscale. Text as elaborated in the post content."
+++

Experimental performance and noise...

Mon 9th December 2019, 8pm  
Access Space    
£5/3 Door/Concession  
No-one turned away for lack of funds
 
### KAZEHITO SEKI (Tokyo, Japan)
https://soundcloud.com/unkodaisuki

Intensely focused extended vocal performance manipulating feedback and the extremes of voice noise through a mixer and flip phone strapped to his chest.

### SW1N-HUNTER (Gateshead, UK)
https://soundcloud.com/swan-hunter

Solo guise of Adam Denton (Trans/Human, TOPH, Swarm Front) conjuring chirps and blurps from some pedals, a pickup, and a magnetic wand.

### GIBLET GUSSET (Sheffield, UK)
https://youtu.be/gBPckMnMIbk

Blackpool car-boot table-top of toys and noise-makers with electric squeal, fizz, and pop from a masked, yelping karaoke chanteuse.

### SHOW DETAILS

Doors will open at 8pm for an 8:20 pm prompt start. We'll aim to be wrapped up by 10pm for transport and weeknight friendliness.

We ask for £5 on the door to help cover artist's travel and expenses. There's a suggested concession rate of £3 if needed but no-one will be turned away for lack of funds.

There won't be a bar but you are welcome to bring your own drinks.

Access Space is an inclusive environment (https://access-space.org/about-2/about/) that has step-free/wheelchair friendly access and toilets. Chairs can be made available upon request. If you have any specific accessibility requirements or questions please feel free to contact me

As an events organiser I am looking to post specific policies but for the meantime we won't tolerate abusive behaviour and want to ensure a safe and inclusive environment for all. You are welcome to raise any concerns with myself before, during, or after the event.

THE HOUSE ORGAN is a DIY label for audio and other artefacts spanning noise and improvisation. For audio releases and more information https://thehouseorgan.xyz

I'd also like to thank Access Space and Alex McClean for their generous support in using the space/facilities.