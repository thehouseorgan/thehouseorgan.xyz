+++
title = "Peter J Woods / Ice Volt / Weird Fields"
date = "2024-04-30"
publishdate = "2024-03-16"
poster = "PJW-IV-WF-GigPoster.png"
image = "PJW-IV-WF-GigSquare.png"
imgalt = "Gig poster. Featuring a photograph of a crudely painted picture which says ‘We All Make Errors’ mounted on an outside wall. All rendered in greyscale. Other text as elaborated in the post content."
+++

Tue 30th April, 8pm\
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)\
BYOB\
£7 ADV / £10 DOOR\
No-one turned away for lack of funds

* https://www.wegottickets.com/event/613739/
* https://www.facebook.com/events/927298205558329

### PETER J. WOODS

Pulling equally from the worlds of harsh noise and absurdist theatre, Peter J. Woods’ solo performances build a sense of terror from simple imagery, muted text, and an unpredictable barrage of silence and sound. Woods utilises these approaches within a critical context, building on his musical research practice to challenge oppressive formations embedded within surrounding musical contexts. Recent projects represent this aesthetic and thematic sensibility through a multimedia approach that includes writing, theatre, concerts, photography, and film. Outside of his solo work, Woods has performed in a variety of punk/metal bands and collaborated with a wide range of other experimental musicians (including Olivia Block, Tatsuya Nakatani, Victor DeLorenzo of The Violent Femmes, and Angel Marcloid to name a few). He also ran FTAM Productions, a record label and concert promotion organisation that hosted the Milwaukee Noise Fest for 8 years.

BANDCAMP: https://peterjwoods.bandcamp.com/

RECENT LIVE SET: https://www.youtube.com/watch?v=jDHErKFHAdQ&t=1s&pp=ygUNcGV0ZXIgaiB3b29kcw%3D%3D

PRESS: https://www.youtube.com/watch?v=ItSNUZIhuUg and https://chicagoreader.com/music/milwaukee-harsh-noise-artist-peter-j-woods-confronts-white-privilege-in-a-multidisciplinary-performance/

WEBSITE: http://peterjwoods.com

### ICE VOLT

minneapolis. est 2006

undead horrors. gurgling echoes of the underdark. spewn through ancient crust. crystalized to jagged spires of frozen brittle needles.

demented vocalisation. grating metal. digital corruption. upset equilibrium.

WEBSITE: ICE VOLT (insidesmusic.com)

AUDIO SAMPLE: http://www.insidesmusic.com/icevolt/icevolt%20-%20portal%20excerpt.mp3

LIVE SET: https://www.youtube.com/watch?v=8B5SpXodviE

### WEIRD FIELDS

Four friends (two Food People, two Krupps) trying to fan the flames of a precious madness.