+++
title = "Liminal Haze w/ Toi Guy / Expanding Foam / Blackcloudsummoner"
date = "2023-06-04"
publishdate = "2023-05-09"
poster = "JD-LH-BCS-GigPoster-2.png"
image = "JD-LH-BCS-GigSquare-2.png"
imgalt = "Gig poster. Paper collage; torn fragments of text laid on top of graph paper. There are a few marks on the graph paper like accidental pencil marks. Everything seems to have been photocopied and so resolution is low and detail lacking. All rendered in greyscale. Text as elaborated in the post content."
+++

Sun 4th June 2023, 7:30pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
BYOB
£7 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/581592/
* https://gath.io/Xj6kfDdSJxdeyAKhHNBdq
* https://www.facebook.com/events/642957210987092

### LIMINAL HAZE w/ TOI GUY

Collaboration between Invisible City and Steep Gloss label heads; Ross - Diurnal Burdens, and Craig - Rovellasca. On tour they are also accompanied by a dancer of which I know little more (and am looking forward to finding out on the night). So far, I count 6 volumes (on Matching Head, Invisible City, Kirigisu, etc.) of bubbling, bucolic field recording and drone.

https://kirigirisurecordings.bandcamp.com/album/volume-6

Joined by Toi Guy, a British-Colombian dance, experimental media and crochet artist based in Newcastle. Toi creates and performs personal/collective transformation portals to worlds where freedom, authenticity, queerness, co-operation and dreams are valuable towards her root ambition of ​healing, resistance to capitalism and reclaiming the human experience. This manifests in choreographed performances, improvisations, and screendance. She also runs a sustainable slow fashion crochet brand ToiToi Tejidos.

https://tourdemoon.com/toi-guy

### EXPANDING FOAM

Expanding Foam has grown out a sculptural art practice to incorporate a range of synthesisers, crude electronics, and found objects. Personal journeys through time, place, and regional histories. Releases on Tone Burst amongst others.

https://expandingfoam.bandcamp.com/album/old-holborns-norfolk-gold

### BLACKCLOUDSUMMONER

Nottingham-based and highly prolific (30-odd releases since 2017). Shards of dirt electronics, thick sludge, and a surface of slime flecked with cosmic dust. BCS has always been somewhat defined by the hints of melody, beauty, and tenderness mingling with the scrape of metal and concrete destruction. Recently, releases have headed in a harsher direction but the emotive breadth remains.

https://blackcloudsummoner.bandcamp.com