+++
title = "Lauren Sarah Hayes / Christopher Haworth / Guy Birkin"
date = "2023-08-06"
publishdate = "2023-07-12"
poster = "LSH-CH-GB-GigPoster.png"
image = "LSH-CH-GB-GigSquare.png"
imgalt = "Gig poster. A miniature amplifier, Korg Monotron, and DIY built Ciat-Lonbarde Lil Sidrassi (housed in an old radio) are huddled together on a large beach rock. The background is filled with further beach rocks and seaweek. All rendered in greyscale. Text as elaborated in the post content."
+++

Sun 6th August, 7:30pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
BYOB
£7 ADV / £10 DOOR
No-one turned away for lack of funds

* https://thehouseorgan.bandcamp.com/merch/lauren-sarah-hayes-christopher-haworth-guy-birkin
* https://www.wegottickets.com/event/587717
* https://gath.io/ugQAp72SHIX5LIToebgbR
* https://www.facebook.com/events/3443741389229448

### LAUREN SARAH HAYES
Lauren Sarah Hayes is a Scottish improviser, sound artist, and scholar who is recognised for her embodied approach to computer music. Her music is a mix of experimental pop/live electronics/techno/noise/free improvisation and has been described as ‘voracious’ and ‘exhilarating’. She is a sculptress of sound, manipulating, remixing, and bending voice, drum machines, analogue synths and self-built software live and physically. She has performed extensively across Europe and the US, including as part of her tenure with the New BBC Radiophonic Workshop at Kings Place, London. Her 2021 release Embrace (Superpang) was included in Bandcamp’s Best Experimental Music of February 2021.
https://www.laurensarahhayes.com/

### CHRISTOPHER HAWORTH
Christopher Haworth is a Sheffield-based composer and musicologist with interests in electronic music and sound art. His deeply experimental work explores ‘systems’, broadly conceived, from technical systems that have some level of mechanical autonomy to the biological system of hearing. A long-standing interest has been in the development of exploratory sound synthesis techniques that utilise auditory distortion products, sometimes termed otoacoustic emissions or difference tones. He is currently Lecturer in Music at the University of Birmingham where he teaches courses in experimental music and sound art, and music perception. Recent work has also appeared on Superpang.
https://christopherhaworth.bandcamp.com/album/auditory-distortion-synthesis

### GUY BIRKIN
Rounding out the contingently-semi-intentional Superpang theme, Guy Birkin is a Nottingham-based artist-musician-researcher with backgrounds in both science and art. Their work investigates complexity as an aesthetic property of music and visual art. The research side of this focuses on the perception and measurement of complexity in audio and visual media, whilst the creative practice leads and supports the research through the production of generative music and visual art. Releases via Fluf, Entr’acte, New York Haunted, Hard Return, Tokinogake, and the previously mentioned Superpang.
https://aestheticcomplexity.wordpress.com/