+++
title = "Rump State / Degradation / Daphnellc"
date = "2023-10-27"
publishdate = "2023-10-03"
poster = "RS-DG-DP-GigPoster.jpg"
image = "RS-DG-DP-GigSquare.jpg"
imgalt = "Gig poster. Strange monolithic structure hanging from top of frame."
+++

Fri 27th October, 8pm
[JT Soar, Nottingham](https://www.openstreetmap.org/way/136702416)
£8 ADV / £10 DOOR
No-one turned away for lack of funds

* https://www.wegottickets.com/event/595744/
* https://www.facebook.com/events/251756594507497
* https://www.rammelclub.org/2023/10/rammel-club-125-rump-state-degradation.html



### RUMP STATE
We find in Rump State Mark Morgan & Gaute Granli. Mark Morgan who was the man behind the too unknown Sightings, a New York trio at the origin of an impressive noise rock of inventiveness, a band difficult to tame and probably too far ahead of its time. A few outstanding records, notably on Load Records, Daïs... and then leave; Mark Morgan has since officiated in Silk Purse with appearances on To Live and Shave in LA and collaborations with Aaron Dilloway, among others.
Gaute Granli is a member of Firmaet Forvoksen. From his native Norway, he distills a confusing and unidentified body of sound as a solo artist... perhaps a new music, a choppy, desolate pop... from the next century.
The two musicians share a guitar playing and a vocal approach of apparently destructured but well and truly chiseled. They produce a sound mass that we feel like drifting or inexorably carried away by quicksand...
Attention: uncomfortable psychedelism and total avant-garde." (Sonic Protest)
https://nolagosmusique.bandcamp.com/album/rump-state-retaliation-aesthetics

### DEGRADATION
Degradation is the duo of D O'Donoghue (Vinegar Tom) and George Rayner-Law (Schwerpunkt) who are also behind forward-thinking experimental/noise cassette label, Brachliegen Tapes.
Their latest release, Leadlined, is a 35-minute lamentation of the perverse ideology of the English, manifested through sound collage and heavy electronics. A carrier bag of improvisation and fragmented assembly, post-industrial synthesis gives way to full-spectrum noise, and aural saturation. Shaped by collapsing signal chains & biting delays, Degradation pore over the rapine chain of degeneration that is England. Through bricolage it attempts to come to terms with the inescapable, morbid nostalgia of the nation's psyche;  society in a state of collapse, reflecting back a withering Britain - plundered, debilitated, and divided.
https://brachliegentapes.bandcamp.com/album/leadlined

### DAPHNELLC
Artist & producer Daphnellc makes experimental techno incorporating field recordings, danceable noise and live drum machines. Ever-changing loops and sounds rotate around ethereal melodies to escape the everyday.
https://lcpmusic.bandcamp.com/album/daphnellc-nonbinary-ep-lc023