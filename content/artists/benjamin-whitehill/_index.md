+++
title = "Benjamin Whitehill"
image = "benjamin-whitehill.jpg"
imgalt = "What appears to be an images of adult students packed into a classroom is heavily distorted by what looks like purposefully manipulated scanning errors."
+++

guitar mangling noise-troubadour and outsider recording artist.