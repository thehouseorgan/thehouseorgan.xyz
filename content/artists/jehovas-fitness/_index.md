+++
title = "Jehova's Fitness"
image = "jehovas-fitness.jpeg"
imgalt = "Jehova's Fitness photographed at a live show. In front of a drum kit, Matthew is kn his knees with his guitar, Murray holding microphone to mouth. Both are blurred by action."
+++

jehovahs fitness is a noise rock band from berlin. we also improvise a bit which means it's made up as we go along so sometimes it’s a bit rubbish but if we realise it’s rubbish we’ll try and stop or make it better so that everyone has a good time. when we sing it’s not really singing either, it’s more like whining, straining, and choking but we think it’s good and also a bit funny too. mathew is a really good guitar player but to make it sound more exciting he’ll play lots of really fast or out of tune things so you’re not quite sure what’s coming next.