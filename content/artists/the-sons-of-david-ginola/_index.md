+++
title = "The Sons of David Ginola"
image = "the-sons-of-david-ginola.jpg"
imgalt = "Photo of The Sons of David Ginola which pictures Kevin Sanders riding on the back of Murray Royston-Ward."
+++

The Sons of David Ginola (SoDG) are Kevin Sanders (London) and Murray Royston-Ward (Nottingham). They use tape loops, mixer feedback, lo-fi electronics, and everyday objects to map electronic terrains of anxiety and tension. Grounded in the domestic and humane, SoDG record at home, at low volume, to create an intimate space for exploring small gestures and improvised electronics. Noise-music embracing its egalitarian, productive, embodied, and sensuous qualities over the unwanted, harsh, loud, and 'other'. 

They have self-released two CD-Rs 'Blood Too Thick Symptoms' and 'Waterside Quests', followed by the 'Silver on Call' cassette on Sndhls.
