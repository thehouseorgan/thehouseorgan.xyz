+++
title = "Murray Royston-Ward"
image = "murray-royston-ward.jpeg"
imgalt = "A snare drum, photographed from above, presents a circle in the centre of the square frame. On top of the drum are a collection of objects including stones, sticks, shells, flowers, and seaweed."
+++

I’ve spent quite a few years involved with improvised, experimental music. I got into making contact mics, DIY recordings, and eventually making small synths and guitar pedals. An approach to materials and creativity which embraces values away from the mainstream. More recently, circuits have been explored for their own sense of liveliness; for the way they can misbehave and produce the unexpected. These are often deployed in feedback configurations where complex systems of circuit/material/me interactions are at play; each seen as an active collaborator in improvised music-making.