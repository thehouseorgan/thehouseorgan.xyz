+++
title = "Nabulsi"
description = "Recorded Autumn 2018 in Nablus, Palestine. All sounds from our balcony on Mount Gerizim, or derived from feedback experiments with contact microphones, vibration speaker, speaker cone, metal objects, solenoid, oscillator, radio tuner, and Dead Sea salt crystal."
date = "2019-02-08"
artists = ["Murray Royston-Ward"]
image = "nabulsi_cover.jpeg"
imgalt = "Nablus at night. Photographed from a vantage point on the hills of Jabal Jirzim a sprawl of illuminated appartment blocks and other buildings fill the lower half of the scene. Above, smokey clouds. The combination of black and white and cloud gives much of the image a slight haze. The word Nabulsi is rendered in the bottom right corner."
year = "2019"
format = []
embed = "991867413"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/nabulsi"
    archive = "https://archive.org/details/MurrayRoystonWardNabulsi"
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1yoBbdAxEYtLwDx626Mfc5XzFE3e_il3e?usp=sharing"
+++

Recorded Autumn 2018 in Nablus, Palestine. All sounds from our balcony on Mount Gerizim, or derived from feedback experiments with contact microphones, vibration speaker, speaker cone, metal objects, solenoid, oscillator, radio tuner, and Dead Sea salt crystal.

During my visit to Nablus, I noticed the large amounts of construction all over the city. In particular, our apartment was in an area of massive development and the sounds of construction dominated the soundscape. For me, these sounds became a way of processing the complexities of Israeli occupation and Palestinian political conditions. Construction may function as a reminder of economic difficulties and attempts to revive or strengthen when political leadership is considered weak and fractured. It may also offer a symbol of hope, building a new future for a continuing Palestine. Construction also reflects the ongoing demolition of Palestinian homes and civic infrastructure by Israeli state forces, alongside the increasing loss of land to ever-expanding illegal settlements.

Of those settlements it is worth mentioning the many reports of settler violence with Palestinians being killed by rocks being thrown at cars, homes terrorised, and several hundred-year-old olive groves, an important source of income and symbols of Palestinian's ties to their land, being destroyed. The settler violence I describe is only that which I encountered within the surrounds of Nablus but is common across the entire occupied West Bank.

This settlement expansion is intertwined with the complexity of the Oslo Accords splitting the West Bank into Areas A, B, and C. Rather than clearly separated zones these areas are difficult to visualise. The 8 major Palestinian cities, such as Nablus, are in Area A and under direct Palestinian control. Israeli citizens are not legally supposed to enter these areas, but military operations continue and settler violence still infringes on these borders. There are buffer zones around these cities, Area B, which remain under Palestinian civil authority but Israeli security control. This area predominantly encompasses Palestinian villages surrounding major cities and are particularly vulnerable to demolition and settler violence. The final area is Area C which currently constitutes ~61% of the West Bank, is under full Israeli authority, and was due to be returned to Palestinian control. Instead, settlement expansion has escalated, with increased support from the Israeli state apparatus, through a variety of legislative changes, new legal frameworks, and state-aided training and armaments for settlers and settlement outposts. The only land, within the West Bank, that Palestinians are free to build on is Area A. For me then, recording from my balcony and mixing in other sounds was a way of beginning to understand and contextualise these surroundings.

All proceeds will be donated to The Palestine Solidarity Campaign - [https://www.palestinecampaign.org/](https://www.palestinecampaign.org/) - a grassroots political organisation campaigning for Palestinian self-determination. Of particular interest to me is the ever expanding illegal Israeli settlements and ongoing settler violence and PSC are are currently campaigning against this.
