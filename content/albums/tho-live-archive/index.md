+++
title = "THO: Live Archive"
description = "This is a 'living' archive of shows put on by The House Organ."
date = "2020-05-07"
artists = ["Various Artists"]
image = "tho-live-archive_cover.jpeg"
imgalt = "Gloomy and grainy black and white photo of a small mixing desk on top of a small table with cables coming out of it. In the background are a bag, small case, and stage light. The background is a block wall with industrial sockets and conduit."
year = "2020"
format = []
embed = "2329052662"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/tho-live-archive"
    archive = ""
    resonate = ""
    spotify = ""
    tidal = ""
    apple = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1vPOcscMsFBFt6gHWlv7_QRRtxwGxkqRK?usp=sharing"
+++

This is a 'living' archive of shows put on by The House Organ. It will be updated and added to on an ongoing basis rather than any one official, or final, release. The updating will of course be on hiatus during 2020, due to Covid-19 pandemic, but hopes to resume in the future.

So far then, these are recordings from the following shows:

- 2019-10-06: LuxuryMollusc / Nacht Und Nebel / Blackcloudsummoner @Jabbarwocky (see [FB event](https://www.facebook.com/events/2345594775531239))

*Unfortunately Blackcloudsummoner's set didn't record (my fault, didn't press the button twice) hence its absence.

- 2019-12-09: Kazehito Seki / SW1n-Hunter / Giblet Gusset @Access Space (see [FB event](https://www.facebook.com/events/415281119353856))

All performances are of course credited to the artists who have kindly allowed these to be released a) for free/PWYL, b) with permissive Creative Commons licensing, and c) also via p2p friendly methods (i.e. torrents).

Recorded and minimally edited, mixed, and mastered by myself (Murray Royston-Ward)
