+++
title = "Don't Give Up"
description = "Live recording from 2019-07-20 at Hatch in Sheffield."
date = "2020-01-24"
artists = ["The Sons of David Ginola"]
image = "dont-give-up_cover.jpg"
imgalt = "Live shot from The Sons of David Ginola performing at Hatch in Sheffield. They are hunched over a table-top of equipment and cables. A phone camera on a tripod is in the foreground. The duo are picked out by dots of light from a projection. All in black and white."
year = "2020"
format = []
embed = "589627206"
cc = "CC-0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/dont-give-up"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1wBSRBQYVGX74j30SoUKHfinbMYf624wz?usp=sharing"
+++

2019-07-20 - Live in Sheffield

For The Sons of David Ginola's first live outing we attempted to honour the video for Peter Gabriel and Kate Bush's 'Don't Give Up'. We animated an eclipse and shared a tender embrace.

Working with the volume of a PA, everything sounded a little more raucous than the quiet domestic recordings we're used to. Regardless, there's still our characteristic pressure cooker stasis, crackles, creaks, and hisses.

Slightly edited and minimally mixed/mastered at home.

As always, it is released without copyright (CC0) http://creativecommons.org/publicdomain/zero/1.0/
