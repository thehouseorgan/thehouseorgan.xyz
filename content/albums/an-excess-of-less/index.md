+++
title = "An Excess of Less"
description = "Two spontaneous recording made on Weserstraße, Neukölln, Berlin in July 2017. They utilise ambient sounds and feedback paths through objects creating slow summer clangs and chatter."
date = "2018-07-06"
artists = ["Murray Royston-Ward"]
image = "an-excess-of-less_cover.jpeg"
imgalt = "A huge pile of slate taken on a walk in Wales. In moody black and white."
year = "2018"
formats = []
embed = "1798803182"
cc = "CC0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/an-excess-of-less"
    archive = "https://archive.org/details/MurrayRoystonWardAnExcessOfLess"
    cc-link = "https://creativecommons.org/publicdomain/zero/1.0/"
    download = "https://drive.google.com/drive/folders/1ywRC50p5yfdHkQfZoSuSIj0oBEN0CmuO?usp=share_link"
+++

Two spontaneous recording made on Weserstraße, Neukölln, Berlin in July 2017. They utilise ambient sounds and feedback paths through objects creating slow summer clangs and chatter.
