+++
title = "Dissolution Matrix in Afterthought of Skies"
description = "6 tracks of mostly angular guitar-based improvised recordings, loose compositions, and archival bits and bobs."
date = "2018-01-01"
artists = ["Murray Royston-Ward"]
image = "dissolution-matrix-in-afterthought-of-skies_cover.jpeg"
imgalt = "In the centre of the frame is a Christian relic; an ornate vessel, reportedly containing a bone fragment of a saint or Jesus or something (I can't remember), and topped by a hand. The image is in black and white with high contrast so the relic stands out against the blackness. Just behind, so small as to be almost indiscercnable, are two crucifixes."
year = "2018"
formats = []
embed = "1955892204"
cc = "CC BY 3.0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies"
    archive = "https://archive.org/details/MurrayRoystonWardDissolutionMatrixInAfterthoughtOfSkies"
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1ywnmjHbF7Oklu9SmksPqozf1A4Jk4IiE?usp=sharing"
+++

This is a mixture of improvised recordings, loose compositions and archival bits and bobs. Snippets of interviews; background noises from gigs and travels; awkward moments from the cutting room floor; twigs rattled in a guitar; shuffling things around rooms; recording ruined by wind; and a never finished 'pop' song.

I've been sitting on it for some time now.

Track 1:
Additional vox: Holly Royston-Ward

Track 2:
Tempelhof wind unintentionally recorded with the assistance of Mathew Johnson (who also let me use the guitar I'm sticking bits of twigs and leaves in and he's still picking them out to this day so mega-thanx)

Track 4:
Additional guitar: Mathew Johnson
Ugandan Harp: Little Bosco

Track 5:
Partner: Holly Royston-Ward
Interviewee: Michael McCann

The work is also privately published in an edition of 25 (see [https://mroystonward.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies](https://mroystonward.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies).

This is a free/donation digital edition intended as archive and alternate distribution.
