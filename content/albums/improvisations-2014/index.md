+++
title = "Improvisations 2014"
description = "Book documenting a series of improvised scenes where the image is the only recording (no audio)."
date = "2016-09-02"
artists = ["Murray Royston-Ward"]
image = "improvisations-2014_cover.jpeg"
imgalt = "Centring the frame are a cymbal, cassette player, and singing bowl with small chain draped over its side. They are nestled amongst tall grasses. All in greyscale."
year = "2016"
format = []
embed = "175827769"
cc = "CC BY 3.0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/improvisations-2014"
    archive = "https://archive.org/details/MurrayRoystonWardImprovisations2014"
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1z7mOzSg16aWkaQtE1xys2mXHxUikxKfR?usp=sharing"
+++

A record of improvisations conducted in various locations throughout 2014.

This is one of the first sets of books I wanted to put out but, having been let down by various printers and suppliers it been on and off hiatus for some time.

Originally stemming from my Masters research this is a series of works dealing with the documentation of improvised audio performance. A photograph of the environment/apparatus is recorded with details of duration, materials, location, date etc. No sound is documented and instead you are invited to exercise 'the ear of the imagination'. Perhaps no sound even happened.

The work has now been privately published in an edition of 50 (see [http://cargocollective.com/mroystonward](http://cargocollective.com/mroystonward)).

This is a free/donation digital edition intended as archive and alternate distribution.
