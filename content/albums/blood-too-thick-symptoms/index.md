+++
title = "Blood Too Thick Symptoms"
description = "2 tracks of quiet, domestic, noise-scapes."
date = "2017-07-01"
artists = ["The Sons of David Ginola"]
image = "blood-too-thick-symptoms_cover.jpg"
imgalt = "Greyscale scene inside Canary Wharf Shopping Centre. The frame is filled with large pixelated sections where the crowd of shoopers have been anonymised using privacy preserving tools."
year = "2017"
format = []
embed = "1109379529"
cc = "CC0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/blood-too-thick-symptoms"
    archive = "https://archive.org/details/TheSonsOfDavidGinolaBloodTooThickSymptoms"
    cc-link = "https://creativecommons.org/publicdomain/zero/1.0/"
    download = "https://drive.google.com/drive/folders/1wmaV4vdgjzXD28axIpLVYaL6H7RWTpBb?usp=sharing"
+++

In 2016 Murray Royston-Ward and Kevin Sanders started started meeting to discuss internet privacy and crypto-friendly publishing.

They also both share overlapping histories through improvised musicking, running DIY record labels and the community perhaps best referred to as the No-Audience Underground.

It is natural that this should lead to recording together: using a range of digital and analog synthesis/processing techniques; so-called 'extended' instrument approaches; DIY circuitry; tape loops; etc.

Recording as 'The Sons of David Ginola', this is the first release from a series of recordings made between late 2016 and early 2017.

It is released without copyright (CC0) http://creativecommons.org/publicdomain/zero/1.0/ and with a corresponding 3" cdr edition of 50.
