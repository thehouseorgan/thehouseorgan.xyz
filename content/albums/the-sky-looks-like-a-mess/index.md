+++
title = "The Sky Looks like a Mess"
description = "Monolithic superimposistion of multiple recordings of weird feedback circuits with a mind of their own."
date = "2021-07-14"
artists = ["Murray Royston-Ward"]
image = "the-sky-looks-like-a-mess_cover.png"
imgalt = "Multiple exposures of the 'Contingent Pre-amp' paper circuit board are overlaid on top of each other. The centre of the image is blurry with mutliple exposures but vague symbols and components can be discerned. Coming off of the central board, a mess of wires but semi-transparent, owing to the layering."
year = "2021"
format = []
embed = "1610686428"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/the-sky-looks-like-a-mess"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1zEvhDk1YAp5ICU7k2yyBbGstU7EkVL1W?usp=sharing"
+++

Whilst building a range of preamps, sourced from various websites, for use with piezo contact microphones, I also randomly prototyped different component values, just to see what would happen. The original designs seemed quite resilient to such changes, still generally working well. I also noticed a shared design pattern and imagined a possible meta-circuit that could also accommodate my variations and open up a space for randomness and contingency. This birthed a project called the CNTNGNT PRMP, a paper circuit designed for radically open DIY circuit building.

I built a random selection of twenty CNTNGT PRMPS, of which only seven worked well enough to use. Each of these seven were tested by simply using a contact mic, through a preamplifier, with a belt-clip mini-amp. I would move the contact mic to find a sweet spot where I could leave it alone and it would exhibit some liveliness, some self-playing moments of instability and movement. I let these play out until the feedback loop had either fallen apart or descended into a single, unmoving tone. I would then continue to search for another sweet spot, rinse, repeat.

Differences in sound and self-playing are generally a result of different gain structures within these preamps, themselves a result of random component selection and inconsistent component manufacturing tolerances. Three circuits, in particular, displayed surprising and non-linear behaviour, responding to my explorations in unusual ways, resisting attempts to find sweet spots, and breaking into unexpected bursts of vibrant sound.

Whilst documenting, I simply put each new recording on a new track within my DAW. When reviewing, I forgot to mute the tracks I didn’t want to hear and was faced with a cacophony of multiple circuits all playing at once. What struck me was that the total excess and exuberance of seven feedback circuits all squealing simultaneously seemed more suggestive of the liveliness inherent in these systems than any one recording alone. I am reminded of Herzog’s jungle, humbled before the overwhelming chaos.
