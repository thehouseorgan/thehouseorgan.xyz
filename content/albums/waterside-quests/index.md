+++
title = "Waterside Quests"
description = "4 tracks of low-key noise."
date = "2018-04-03"
artists = ["The Sons of David Ginola"]
image = "waterside-quests_cover.jpeg"
imgalt = "A wall of ship figureheads at Greenwhich Maritime Museum."
year = "2018"
format = []
embed = "2403393501"
cc = "CC0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/waterside-quests"
    archive = "https://archive.org/details/TheSonsOfDavidGinolaWatersideQuests"
    cc-link = "https://creativecommons.org/publicdomain/zero/1.0/"
    download = "https://drive.google.com/drive/folders/1w5krhm6lPDkm9a8xOQrLWGP2QLg9YEJO?usp=sharing"
+++

In 2016 Murray Royston-Ward and Kevin Sanders started started meeting to discuss internet privacy and crypto-friendly publishing.

They also both share overlapping histories through improvised musicking, running DIY record labels and the community perhaps best referred to as the No-Audience Underground.

It is natural that this should lead to recording together: using a range of digital and analog synthesis/processing techniques; so-called ‘extended’ instrument approaches; DIY circuitry; tape loops; etc.

Recording as ‘The Sons of David Ginola’, these documents are taken from improvisations made over the past 18 months in South Norwood.
