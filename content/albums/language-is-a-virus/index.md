+++
title = "Language Is a Virus"
description = "A sound collage of cut-ups and booklet drawing from my wife's time in Sierra Leone during an ebola outbreak."
date = "2016-07-29"
artists = ["Murray Royston-Ward"]
image = "language_is_a_virus_cover.jpeg"
imgalt = "Halftone dotted black and white photo taken in Sierra Leone. A tall wall has the words 'STOP! IMMIGRATION CHECK POINT' painted on it. Below is a large poster saying 'EBOLA IS REAL. PROTECT YOURSELF, PROTECT YOUR FAMILY, PROTECT YOUR COMMU' (with community running out of frame)."
year = "2016"
format = []
embed = "1939848737"
cc = "CC BY 3.0"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/language-is-a-virus"
    archive = "https://archive.org/details/MurrayRoystonWardLanguageIsAVirus01LanguageIsAVirus"
    cclink = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1z7AGYEyxVkxvIzs1EpR7h6ZCX4p_WQ6N?usp=sharing"
+++

In early 2016 I was invited to take part in an exhibition exploring post colonial narratives. The plan was to utilise DIY audio transducers to activate the window of the gallery as a speaker.

In 2015 my wife worked in Sierra Leone managing an Ebola Holding Centre. Her experiences highlighted numerous problematic narratives: the patient handling protocols themselves; racist and fundamentalist Christian histories; media exaggerations of 'African-ness'; local acts of mistrust and misunderstanding.

Taking Burroughs' suggestion that 'language is a virus' I worked on a tape cut up, using a wide range of sources and narratives surrounding Ebola, exploring the mutations and vectors of transmission for the ebola-word-virus.

The intention was for this cut-up to be played back, via a reel-to-reel tape player customised with a raspberryPi, into the window, radiating out of the gallery into the street.

Unfortunately the originally proposed exhibition fell through and this project, along with artist books produced to coincide with the show, has lay dormant.

The work has now been privately published in an edition of 30 (see [http://cargocollective.com/mroystonward](http://cargocollective.com/mroystonward)).

This is a free/donation digital edition intended as archive and alternate distribution.
