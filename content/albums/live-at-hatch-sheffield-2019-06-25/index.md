+++
title = "Live at Hatch, Sheffield, 2019/06/25"
description = "Live recording from 25th June 2019 at Hatch in Sheffield."
date = "2020-01-24"
artists = ["Murray Royston-Ward"]
image = "live-at-hatch-sheffield-2019-06-25_cover.jpg"
imgalt = "Cluttered table-top of electronic audio equipment, wires, metal objects, cellotape, e-bow, and rubber band. In the background, a guitar is resting across the tops of two drums. A singing bowl is balanced on the guitar strings."
year = "2020"
format = []
embed = "444921635"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/live-at-hatch-sheffield-2019-06-25"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1z5mqrq1wmrPLwcwwCmJmeGx2PLjldem6?usp=sharing"
+++

Live recording from 25th June 2019 at Hatch in Sheffield.

Using contact mic's, vibration transducers, a solenoid, and various electronics, sounds are routed through two drums, a guitar, megaphone and the PA to create various physical feedback paths through objects and space. Improvisation which follows various material flows.

Thanks to RekLaw@Hatch for the live recording and Eimear for arranging the show and asking me to play.

Edits and mastering were done at home by Murray Royston-Ward.
