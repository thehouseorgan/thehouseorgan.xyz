+++
title = "Stop Stroking Your Chin!"
description = "An 11 track mini-album of messy guitar and mouth noises."
date = "2019-04-06"
artists = ["Jehova's Fitness"]
image = "stop-stroking-your-chin_cover.jpg"
imgalt = "On a white background, a jaunty angled black and white crab holds a cigarette. Below it, in messy, black ink, the band name and album title are daubed."
year = "2019"
format = []
embed = "1085003397"
cc = "CC-BY"
[listen]
    bandcamp = "http://thehouseorgan.bandcamp.com/album/stop-stroking-your-chin"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1rfdp82svFG1E2B9t4s7xVcr0AAt3laDy?usp=sharing"
+++

this is a mini-album (which is what you're meant to call an album that is shorter than a normal album) by jehova’s fitness, a noise rock band from berlin. that means we (mathew johnson and murray royston-ward who are writing this about our band) play songs but they’re a bit messy and chaotic at times. we also improvise a bit with our songs which means they’re made up as we go along so sometimes it’s a bit shit but we edited it all so most of the shit bits have been taken out so it's more fun. we recorded at the funkhaus in berlin because mathew works there and it didn't cost us any money. when murray sings it’s not really singing, it’s more like whining, straining, and choking, what phil minton calls “mucking about with your mouth”, but we think it’s good and also a bit funny too. mathew is a really good guitar player but to make it sound more exciting he’ll play lots of really fast or out of tune things so you’re not quite sure what’s coming next. we'll be going on a short tour in the summer so come and watch us if we're in your town, or you feel like travelling to another town.
