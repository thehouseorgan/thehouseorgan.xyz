+++
title = "Palace der Republik"
description = "7 tracks exploring various guitar and electronics processes utilising feedback and all recorded in a large corridor at Funkhaus, Berlin."
date = "2019-11-08"
artists = ["Benjamin Whitehill", "Murray Royston-Ward"]
image = "palace-der-republik_cover.jpeg"
imgalt = "Greyscale double exposure of trees and water from around Treptow, Berlin"
year = "2019"
format = []
embed = "3033189020"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/palace-der-republik"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/1ywDq3Xxvn6K9D_CKTo-WBk5xaXS6X6yX?usp=sharing"
+++

Ben and Murray met in Berlin in the Summer of 2018 and discovered a mutual interest in using vibration speakers to create physical feedback paths through their respective instruments. Ben utilises a range of extended guitar techniques, vibration speakers, fx pedals, and a DIY built amplifier which incorporates radio interference. Murray makes feedback systems through junk objects, architectural features such as windows, balloons, and sellotape using an array of contact mic's, transference through proximity, audio exciters, and vibration speakers. A selection of audio electronics introduce dynamic re-routing of signal paths, stray radio signals, solenoids, motors, and oscillators.

Recorded my Mathew Johnson ([www.soundengineer-berlin.com](https://www.soundengineer-berlin.com)) at Funkhaus Berlin in the summer of 2018

Mixed and Mastered by B Whitehill and M Royston-Ward
Album description here
