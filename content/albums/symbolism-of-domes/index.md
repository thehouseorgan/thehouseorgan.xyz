+++
title = "Symbolism of Domes"
description = "4-track EP of weird remote collaboration artefacts."
date = "2023-07-12"
artists = ["Jehova's Fitness"]
image = "symbolism-of-domes_cover.png"
imgalt = "A very basic line drawing scrawl of a rudimentary dome. The band name (Jehova’s Fitness) and title (Symbolism of Domes) are written in the lower half weirdly running vertically for no reason other than it’s goofy."
year = "2023"
format = []
embed = "3480408535"
cc = "CC-BY"
[listen]
    bandcamp = "https://thehouseorgan.bandcamp.com/album/symbolism-of-domes"
    archive = ""
    cc-link = "https://creativecommons.org/licenses/by/3.0/"
    download = "https://drive.google.com/drive/folders/13ktfaOlr-q5sQi8Fg4Hw_86DenQbVWLy?usp=sharing"
+++

seeing as we live in different countries now, we thought we'd try making some tracks by sharing files. i dunno, it's kinda hard to do properly when you're not in a room together and making the time to hang out. i mean, it's not like this is a job or anything, it's just meant to be a bit of fun for two friends spending time together. so, anyway, after several stops and starts, some hiatuses, a break or two, and sitting on these tracks for a number of months, here we are. different to the previous releases and shows, still noisy and silly, and very much the result of trying a thing to see if it works and deciding it probably doesn't but these are fun anyway so lets chuck em into the wild and see if anyone listens. over to you.
