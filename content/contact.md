+++
title = "Contact"
description = "Contact details for The House Organ"
image = "arrgh.jpeg"
imgalt ="Picture of me, all hipster doofus-like with moustache and perma-beanie. I'm wrapped up warm, standing infront of water and harbour industrial buildings in Svendborg, Denmark. A badly scrawled speech bubble has been added (via phone image markup-vibes) saying 'ARRGH!!' and the whole thing is in moody greyscale to match the website aesthetic."
+++

Releases, gigs, trades, community, etc., it'd be great to say hi.

Feel free to drop me an email @ murray [at] this website (so, @thehouseorgan.xyz).

Or, you can use the bandcamp contact form - https://thehouseorgan.bandcamp.com. 'Following' on Bandcamp would be cool too (where I'll send out infrequent updates).

I'm only on limited social media but mastodon would work for a DM - [@trippingonwires@post.lurk.org](https://post.lurk.org/@trippingonwires)

It's not for everyone but I'm totally cool with talking on the phone or using signal for messaging (but you'll need to get in touch first to share phone numbers).

I really getting back into the idea of RSS feeds again and, thankfully, Hugo (which I used to make the website) makes this pretty easy. To keep track of new releases, [follow in your feedreader of choice]({{< ref path="/albums/" outputFormat="rss" >}}). This is pretty basic at the moment but I hope to add a few tweaks and updates soon.

I briefly had a mailing list which is on pause. I'll put it here if/when I revive it.