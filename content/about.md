+++
title = "About"
description = "About - Get to know The House Organ"
+++

The House Organ is a DIY label for self-published music (and sometimes gigs).

### A Potted History

Never a straight path. The House Organ emerged from the ashes of [The Lows and the Highs Records](https://www.discogs.com/label/295969-The-Lows-and-the-Highs-Records) (TLATH). TLATH itself was a tape/cdr label; existing from when I lived in Loughborough, to moving to Cardiff. I didn't understand at the time, but I'd ran out of steam for putting out other peoples music. Instead, I wanted to focus on more of a 'vanity' label, for myself and possible collaborations.

This basic impetus remains unchanged. It also informs the name, The House Organ; a kind of in-house magazine or trade publication.

Where this path veers into many muddy tracks coincides with my  interest in privacy issues and the basic health of the world-wide web. [Edward Snowden's](https://www.eff.org/deeplinks/2014/06/65-65-things-we-know-about-nsa-surveillance-we-didnt-know-year-ago) revelations perturbed me; online spaces plummeted away from freedom and promise. Instead, hurtling towards the great data-mining enterprises of ad-platforms.
I also recognise these shifts within [Cory Doctrow's enshittification of the internet](https://pluralistic.net/2022/11/28/enshittification/#relentless-payola).

~~Whilst never part of the original plan, The House Organ has also ended up as a banner for sporadic gigs. I seem to have recenly resumed promoting shows in Nottingham and there's now a [Live](https://thehouseorgan.xyz/gigs/) section on this website.~~

UPDATE: Live shows are back on hiatus owing to time, energy, etc.

### Crypto

I must preface this as, crypto-now is different to crypto-then. Crypto-now is the worst. From crypto-currencies to NFTs, speculative-market-tech-bro-finance bullshit, and the climate destroying energy needs accompanying it.

Crypto-then was about using encryption tools to fight data-harvesting of every online interaction. It felt anti-capitalist and promising. I was also captured by peer-to-peer (p2p). Torrents and the like for self-publishing without relying on centralised tech platforms.

Unfortunately this never worked out as hoped. Much of the tech-side alienated people. It had the result of making things harder for most. The battle is lost. The war is over. Big-tech has won. Plus crypto became the worst. Enough said.

### Streaming

I avoided streaming platforms; instead focusing upon crypto and other alternatives. Now though, I have mellowed (a little).

Let's face it, streaming is terrible for artists ~~but hey, it's where people are listening. I've been making my music accessible in as many places as possible. Meeting the audience halfway. As much as~~ I hate feeding the Spotify machine, ~~I get a massive kick from the odd random stream. Someone in Chicago has decided to listen to [Jehova's Fitness](http://jehovasfitness.thehouseorgan.xyz); AMAZING!!!~~

Even as I update this (in April 2023), the landscape has ruptured in the past couple of years. Before, I'd point to more artist friendly alternatives.

~~[Resonate](https://resonate.is/) is a co-op streaming platform. Unfortunately it's future seems uncertain and is likely to dissolve soon.~~

UPDATE: Resonate is basically dead now.

~~[Ampled](https://www.ampled.com/) is a co-op patronage site. It's never suited me though and is an alternative to, e.g., Patreon, rather than streaming.~~

UPDATE: Ampled is dead now.

Buying on [Bandcamp](https://bandcamp.com/) still supports artists directly. It's now owned by ~~Epic~~ Songtradr (which doesn't have to be a bad thing but it makes many of us nervous). I've always been dubious about putting all our eggs in one centralised-corporate-basket. [Workers have recently unionised though, yay!](https://www.bandcampunited.org).

~~I'm also taking influence from [POSSE](https://indieweb.org/POSSE) (though not the actual technical steps to implement it). Technically, using streaming services doesn't quite fit this but, I'm embracing the principle of letting you all keep using your prefered sites/apps and making music availible to you there.~~

UPDATE: You'll notice some major edits above. I'm back off streaming. I can't justify the distrokid fees (not to mention they refuse to pay the measly royalities I've earned) anymore and there's no tolerable service to get music up on streaming platforms so I'm done with it.


### Pricing

I changed the pricing on Bandcamp a couple of years ago. There's a long history amongst my peers for pay-what-you-like (PWYL) pricing. I've decided to drop this. I don't like it. I think that there's an unsustainable tendency for people not to get paid for their work. Releasing music and playing shows raises such questions of value. None of this is a commercial endeavour but, there's basic maths of affordability and sustainability. Album/gig prices should reflect that. I could say much more but I won't, it's my take and I'm sticking to it.


### Free

Despite what I've said above, PWYL and NOTAFLOF (no-one turned away for lack of funds) are prevalent for a reason. Scarce and precarious employment, high costs of living, and genuine attempts at accessibility. I first wrote this before Covid. Now things are even worse!!!

Paying people for their work should actually help with these issues in the long-term. In the short-term people still need to catch a break.

You can't download from THO's Bandcamp for free anymore but, there *should* always be a free download link on this website.

This was via a direct browser download  using [Keybase](https://keybase.io/). Since they got acquired by Zoom I've ditched this. It's not ideal but there should now be a Google Drive share for everything. You don't need an account to download but they probably track you whilst you're doing it. Sorry. Get in touch for a private alternative share.

In some cases there are also downloads via [archive.org](https://archive.org/).


### Socials

~~I have a bit of a love/hate relationship with social media (who doesn't). Still, trying to embrace [POSSE](https://indieweb.org/POSSE), I'm trying to share stuff everywhere to meet you all wherever you are. The thing is, I also make noisy circuits as [Tripping on Wires](https://trippingonwires.com) and really can't manage to handle multiple accounts for multiple purposes. Therefore, there'll be a bit of overlap between accounts.~~

- ~~[Mastodon (as Tripping on Wires)](https://post.lurk.org/@trippingonwires)~~
- ~~[Instagram (as Tripping on Wires)](https://www.instagram.com/trippingonwires)~~
- ~~[Twitter (as Tripping on Wires)](https://twitter.com/trippingonwirez)~~
- ~~[Facebook (as The House Organ)](https://www.facebook.com/thehouseorgannotts)~~

UPDATE: I've finally pulled the plug on all social media and couldn't be happier with my life choices.


### Fediverse

~~Don't know if you noticed above but... Mastodon.~~

~~You'll probably have not missed the take over of Twitter by the big-nazi-space-baby. It's imminent demise is probably a bit overstated but, it has revealed just how fucked and fragile it all is.~~

~~Mastodon is a good alternative but I can't see that much of my community has migrated. It does seem to put people off and seem somewhat daunting at first.~~

~~I do think that people overthink the choice of server (it's easy to move home) and it's worth just jumping in, but I'm not here to tell you what to do. Come over if you like though, you're more than welcome.~~

~~If you're interested the broader picture of the fediverse, here's a great resource on [monoskop](https://monoskop.org/Fediverse)~~

UPDATE: Whilst probably a good idea I'm off the socials (see above) and so this seems irrelevant now.

__The House Organ: In-house audio reports from the amateur avant-garde.__