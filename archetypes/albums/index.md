+++
title = "{{ replace .Name "-" " " | title }}"
date = "{{ dateFormat "2006-01-02" .Date}}"
artists = [""]
image = "{{ .Name }}_cover"
year = ""
format = []
embed = ""
cc = ""
[listen]
    bandcamp = ""
    archive = ""
    resonate = ""
    spotify = ""
    tidal = ""
    apple = ""
    cc-link = ""
    keybase = ""
+++

Album description here
